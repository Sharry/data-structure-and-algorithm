"""ch2_selection_sort.py"""


def selection_sort(array: [int]) -> None:
    """selection_sort"""
    # left is sorted area
    for i, _ in enumerate(array):
        smallest_index = i
        for j in range(i, len(array)):
            if array[j] < array[smallest_index]:
                smallest_index = j
        if smallest_index != i:
            array[i], array[smallest_index] = array[smallest_index], array[i]


data = [3, 2, 1]
selection_sort(data)
print(data)

data = [4, 5, 1, 2, 3]
selection_sort(data)
print(data)
