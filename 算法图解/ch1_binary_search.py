"""ch1_binary_search.py"""


def binary_search(array: [int], target: int) -> int:
    """binary_search"""
    low, high = 0, len(array) - 1

    while low < high:
        middle = (low + high) // 2
        if array[middle] == target:
            return middle
        if array[middle] < target:
            low = middle + 1
        else:
            high = middle - 1
    return -1


print(binary_search([1, 2, 3, 4, 5, 6], 5))
print(binary_search([1, 2, 3, 4, 5, 6], 7))
print(binary_search([1, 2, 3, 4, 5, 6], -1))
