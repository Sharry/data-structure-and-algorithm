"""ch4_quick_sort.py"""


def partition(array: [int], left: int, right: int) -> int:
    """partition"""
    # 只要保证左边已排序的数组内都是比 pivot 小的值，那么右侧自然都是比 pivot 大的值。
    partition_index = right
    i = left
    for j in range(left, right):
        if array[j] < array[partition_index]:
            array[i], array[j] = array[j], array[i]
            i += 1
    array[i], array[partition_index] = array[partition_index], array[i]
    return i


def quick_sort(array: [int], left: int, right: int):
    """quick_sort"""
    if left > right:
        return

    partition_index = partition(array, left, right)
    if partition_index > 1:
        quick_sort(array, left, partition_index - 1)
    if partition_index + 1 < right:
        quick_sort(array, partition_index + 1, right)


test_data = [3, 2, 1]
quick_sort(test_data, 0, len(test_data) - 1)
print(test_data)

test_data = [4, 5, 1, 2, 3]
quick_sort(test_data, 0, len(test_data) - 1)
print(test_data)

test_data = [2, 1]
quick_sort(test_data, 0, len(test_data) - 1)
print(test_data)
