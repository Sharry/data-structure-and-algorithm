"""lru"""

# 维护一个有序单链表，越靠近尾部的节点是越早之前访问的。
# 1.如果当前访问的数据已经存在列表中，则把他插入到头部。
# 2.如果没有缓存过当前访问的数据，则执行以下操作
#     1.缓存未满，直接插入到头部
#     2.缓存已满，删除尾部节点，新节点插入到头部


class LinkedListNode:
    """LinkedListNode"""

    def __init__(self, value, next_node) -> None:
        self.value = value
        self.next_node = next_node


def len_of_linked_list(head: LinkedListNode) -> int:
    """len_of_linked_list"""
    length = 0
    temp = head
    while temp is not None:
        length += 1
        temp = temp.next_node
    return length


def print_linked_list(head: LinkedListNode) -> None:
    """print_linked_list"""
    temp = head
    while temp is not None:
        print(f"{temp.value}->", end=" ")
        temp = temp.next_node
    print("NULL")


def delete_node(head: LinkedListNode, value: int) -> None:
    """delete_node"""
    prev = head
    temp = prev.next_node
    while temp is not None:
        if prev.next_node is not None and prev.next_node.value == value:
            prev.next_node = prev.next_node.next_node
        prev = prev.next_node
        temp = temp.next_node


def insert_node_as_head(head: LinkedListNode, value: int) -> LinkedListNode:
    """insert_node_as_head"""
    return LinkedListNode(value, head)


def delete_tail_node(head: LinkedListNode) -> None:
    """delete_tail_node"""
    temp = head
    while temp.next_node is not None and temp.next_node.next_node is not None:
        temp = temp.next_node
    temp.next_node = None


def get_item(head: LinkedListNode, value: int, max_len: int = 5) -> LinkedListNode:
    """get_item"""
    temp = head
    while temp is not None:
        if temp.value == value:
            delete_node(head, value)
            return insert_node_as_head(head, value)
        temp = temp.next_node
    if len_of_linked_list(head) == max_len:
        delete_tail_node(head)
    return insert_node_as_head(head, value)


data = LinkedListNode(1, LinkedListNode(2, LinkedListNode(3, None)))
data = get_item(data, 5)
print_linked_list(data)
data = get_item(data, 4)
print_linked_list(data)
data = get_item(data, 3)
print_linked_list(data)
data = get_item(data, 2)
print_linked_list(data)
data = get_item(data, 3)
print_linked_list(data)
data = get_item(data, 4)
print_linked_list(data)
data = get_item(data, 6)
print_linked_list(data)
data = get_item(data, 7, max_len=8)
print_linked_list(data)
